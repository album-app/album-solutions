---
slug: "/solution-example"
title: "Example solution"
---

An Album solution is a single Python file. It includes all metadata and entry points to installing and launching the solution via a joint `setup` method provided by the Album API.

The only mandatory arguments of the setup method are `group`, `name`, `version` and `album-api-version`. `group`, `name`, and `version` build the identifier of the solution. 

Here is an example with (almost) all available solution setup parameters.

