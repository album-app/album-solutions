---
slug: "/install_gui"
title: "Album graphical user interface"
description: ""
next: ["run_solution_from_catalog"]
---
**NOTE**

The graphical user interface of Album just recently hatched and is experimental. There will likely be issues or features you are missing - please let us know either on this page below in the comment section or [here](https://gitlab.com/album-app/plugins/album-gui/-/issues). Your feedback is highly appreciated.

## Start from scratch with the Album Installer
Install Album as described [here](https://docs.album.solutions/en/latest/installation-instructions.html) using the installer wizard - this will generate a launcher / a shortcut on your desktop (depending on your operating system) for the Album user interface.


### What happens behind the scenes?
The Album installer is a one file executable that automatically takes care of the following steps:
1. It creates a folder called `.album` in your home directory, which will hold all Album related files.
2. It installs Micromamba into `.album/micromamba`.
3. It installs the Album environment into `.album/envs/album`.
4. It creates a shortcut for launching the graphical user interface of Album.


## How to use the Album interface
1. The Album launcher should be located either in your list of applications or as a shortcut on your desktop. Once you launch it, you should see a list of solutions coming with the default Album catalog - these are templates which are not particularly interesting to run.

2. Add catalogs by clicking on `catalogs` in the bottom left corner and then on `Add catalog`. Enter the URL of the catalog, for example `https://gitlab.com/album-app/catalogs/helmholtz-imaging`.

When running a solution which is not installed, the UI will ask you to install the solution first.
