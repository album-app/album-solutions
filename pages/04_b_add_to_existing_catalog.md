---
slug: "/add_to_catalog"
title: "Add your solution to an existing catalog"
description: "Click here to learn how to upload your solution to an already existing catalog."
---

<div class="learning-objectives">
Learning objectives:
<ul>
<li>learn what is necessary to deploy to a catalog managed by someone else.</li>
</ul>
</div>

---

In the last section you learned how to create your own remote catalog and deploy a solution to it.
But you can also deploy your solution into an already existing catalog.

Like for example the catalog of a coworker, the group you work in or the institute you work for.

<a name="04btask1"></a>
<div class="task">
<div class="task-title">Task 1 - Get access to a target catalog that is not yours. Work in groups if needed. </div>
Ask to become a collaborator for a given catalog:
- To be able to deploy a solution into a catalog your git user needs the permission to write into the git repository
of the catalog. You need to be a collaborator of the catalog.

</div>


<a name="04btask2"></a>
<div class="task">
<div class="task-title">Task 2 - Add your catalog to your album installation!</div>

Add the catalog to your collection:
- For Album to recognise your new catalog you have to add it to your local collection of catalogs 
  with the following command:
  ```
  album add-catalog [repo-url]
  ```
</div>

<a name="04btask3"></a>
<div class="task">
<div class="task-title">Task 3 - Deploy your solution to the catalog!</div>

Deploy your solution into the catalog:
- You can now address the catalog by the name it carries and deploy solutions to it
- Simply run the following command:
  ```
  album deploy [solution-path] [catalog-name]
  ```
</div>

<a name="04btask4"></a>
<div class="task">
<div class="task-title">Task 4 - Don't forget to update and upgrade your Album!</div>

Upgrade your local version of the catalog
- Your solution is now part of the remote catalog (e.g. check it in github/gitlab). To synchronise
  your local catalog managed by Album with the remote, run the following commands:
  ```
  album update [catalog-name]
  album upgrade [catalog-name]
  ```
</div>
