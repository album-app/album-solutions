---
slug: "/local_catalog"
title: "Create and populate a local catalog"
description: "Collecting internal software routines? Click here to learn how to setup a local catalog, for example on a network drive, and how to add your solution."
---
<div class="learning-objectives">
Learning objectives:
<ul>
<li>learn how to share a catalog without a URL</li>
</ul>
</div>

---


If you want to collect your solutions inside a catalog but don't want to make it public you can create a local catalog,
for example on your local hard drive or a network drive. 

<a name="04ctask1"></a>
<div class="task">
<div class="task-title">Task 1 - Create a catalog that lives on your local hard drive. </div>

Create a new catalog on your hard drive:
- it is as easy as:
   ```
   album clone template:catalog [catalog-dir] [catalog-name]
   ```

</div>



<a name="04ctask2"></a>
<div class="task">
<div class="task-title">Task 2 - Add your catalog to your album installation!</div>

Add the catalog to your collection:
- For Album to recognise your new catalog you have to add it to your local collection of catalogs 
  with the following command:
  ```
  album add-catalog [repo-url]
  ```
</div>


<a name="04ctask3"></a>
<div class="task">
<div class="task-title">Task 3 - Deploy your solution to the catalog!</div>

Deploy your solution into the catalog:
- You can now address the catalog by the name it carries and deploy solutions to it
- Simply run the following command:
  ```
  album deploy [solution-path] [catalog-name]
  ```
</div>

<a name="04ctask4"></a>
<div class="task">
<div class="task-title">Task 4 - Don't forget to update and upgrade your Album!</div>

Upgrade your local version of the catalog
- Your solution is now part of the remote catalog (e.g. check it in github/gitlab). To synchronise
  your local catalog managed by Album with the remote, run the following commands:
  ```
  album update [catalog-name]
  album upgrade [catalog-name]
  ```
</div>
