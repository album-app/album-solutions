import * as React from "react"
import { StaticImage } from "gatsby-plugin-image"

export default function MacOSLogo() {
  return (
  <StaticImage className="logo" alt="linux logo" src="../images/macos-logo.png" imgStyle={{ objectFit: "contain", placeholder: "blurred" }}/>
  );
}
