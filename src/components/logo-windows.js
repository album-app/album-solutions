import * as React from "react"
import { StaticImage } from "gatsby-plugin-image"

export default function WindowsLogo() {
  return (
  <StaticImage className="logo" alt="linux logo" src="../images/windows-logo.png" imgStyle={{ objectFit: "contain", placeholder: "blurred" }}/>
  );
}
