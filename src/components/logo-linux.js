import * as React from "react"
import { StaticImage } from "gatsby-plugin-image"

export default function LinuxLogo() {
  return (
  <StaticImage className="logo" alt="linux logo" src="../images/linux-logo.png" imgStyle={{ objectFit: "contain", placeholder: "blurred" }}/>
  );
}
